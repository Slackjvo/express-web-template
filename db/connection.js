import mysql from 'mysql2'
import dotenv from 'dotenv'
dotenv.config()

const WEB_ID = process.env.WEB_ID || 1

//Credentials
const invitedPool = mysql.createPool({
	host: process.env.HOST,
	user: process.env.INVITED_USER,
	password: process.env.INVITED_PASS,
	database: process.env.DB_NAME,
	connectionLimit: 40,
	waitForConnections: true,
	queueLimit: 10000
}).promise()

const updatePool = mysql.createPool({
	host: process.env.HOST,
	user: process.env.UPDATE_USER,
	password: process.env.UPDATE_PASS,
	database: process.env.DB_NAME,
	connectionLimit: 15,
	waitForConnections: true,
	queueLimit: 10000
}).promise()

const getConnection = async(pool="invited") => (pool === "update" ? await updatePool.getConnection() : await invitedPool.getConnection())

const query = async(querySQL, params=[], pool="invited") => {
	try{
		const [results,fields] = (pool === "update" ? await updatePool.query(querySQL,params) : await invitedPool.query(querySQL,params))
		return [results,fields]
	}catch(err){
		console.log(err)
	}
}

export {getConnection,query,WEB_ID}