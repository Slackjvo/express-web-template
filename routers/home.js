import express from 'express'
import path from 'path'
import fs from 'fs'
import {requestLog} from '../src/utils'

const router = new express.Router()

router.get('/',	async(req,res) => {
	const file = 'index'
	try{
		res.render(file, {title: `Title`})
	}catch(err){
		console.log('error: ', err);
		res.end();
	}
	requestLog(req,res,req.startTimeRequestLog)
})

//Sitemaps
router.get('/sitemp:idSitemap?.xml', async(req, res) => {
	const pathSitemapFile = path.resolve(process.cwd()+`/sitemaps/sitemp${req.params.idSitemap}.xml`)
	fs.access(pathSitemapFile, fs.constants.F_OK, (err) => {
		if(err)	res.redirect('/');
		else	res.sendFile(pathSitemapFile);
	});
	requestLog(req,res,req.startTimeRequestLog)
})

export default router