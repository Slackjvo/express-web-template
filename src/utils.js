import fs from 'fs'
import sgMail from '@sendgrid/mail'

sgMail.setApiKey(process.env.SENDGRID_API_KEY)
const isProd = parseInt(process.env.PROD)

async function sendEmail(toEmai, fromEmail, subject, htmlMessage){
	const msg = {
		to: toEmail,
		from: fromEmail,
		subject: subject,
		html:htmlMessage,
	}
	try {
		await sgMail.send(msg)
		return [true,'']
	} catch (e) {
		return [false,e]
	}
}

const getActualRequestDurationInMilliseconds = (startTime) => {
	const NS_PER_SEC = 1e9 //  convert to nanoseconds
	const NS_TO_MS = 1e6 // convert to milliseconds
	const diff = process.hrtime(startTime)
	return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS
}

//Express Logger
const requestLog = (req,res,startTime) => {
	if(false){
		try{
			const currentDatetime = new Date()
			const formattedDate =	currentDatetime.getFullYear() + "-" + (currentDatetime.getMonth() + 1) + "-" + currentDatetime.getDate() + " " + currentDatetime.getHours() + ":" + currentDatetime.getMinutes() + ":" + currentDatetime.getSeconds()
			const durationInMilliseconds = getActualRequestDurationInMilliseconds(startTime)
			if (startTime && ((durationInMilliseconds/1000) > 5)) {
				let log = `IP: ${req.ip} | Original url: ${req.originalUrl} | [${formattedDate}] ${req.method}:${req.url} ${res.statusCode} ${durationInMilliseconds.toLocaleString()} ms | ${(durationInMilliseconds/1000).toLocaleString(undefined, {maximumFractionDigits: 2})} s`
				if(!isProd){
					fs.appendFile("/home/sexixo-logs/sexixo.com.requests_log", log + "\n", err => {
						if (err)	console.log(err)
					})
				}else{
					sendEmail(`${req.webName} - Request Muy Larga`, `<strong>${log}</strong>\n\nPost Data Sent:\n${JSON.stringify(req.body)}`)
				}
			}
		}catch(err){
			console.log(err)
		}
	}
}

export {requestLog}