import express from 'express'
import useragent from 'express-useragent' 
//import helmet from 'helmet'
//import session from 'cookie-session'
import {query,WEB_ID} from '../db/connection.js'
//import path from 'path'

//Routes
import homeRouter from '../routers/home.js'
//import api from '../routers/api.js'

const isProd = parseInt(process.env.PROD)

//Port
const port = process.env.PORT

// BlackList ips
const BLACKLIST = process.env.BLACKLIST.split(',')

const getClientIp = (req) => {
	let ipAddress = req.socket.remoteAddress;
	if (!ipAddress) {
		return '';
	}
  	// convert from "::ffff:192.0.0.1"  to "192.0.0.1"
	if (ipAddress.substr(0, 7) == "::ffff:") {
		ipAddress = ipAddress.substr(7)
	}
  	return ipAddress;
}

//Get Web Data and Start server
const startServer = async () => {
	try{
		//Express server
		const server = express()

		//Get Info of web
		//const [webDataRow,fieldsWebData] = await query('Select * from webs where id = ?', [WEB_ID])
		//const webData = webDataRow[0]
		server.locals.web = {}
			
		// Cookies
		/*const expiryDate = new Date( Date.now() + 60 * 60 * 24000) //24 hour
		server.use(session({
			name: 'session',
			keys: ['key1', 'key2'],
			cookie: { secure: true,
						httpOnly: true,
						domain: 'sexixo.com',
						path: '/',
						expires: expiryDate
					}
			})
		)*/

		//listen for requests
		server.listen(port, () => {
			console.log(`Server listening for port ${port}`)
		})

		//register view engine
		server.set('view engine', 'ejs')

		//middleware & static files
		server.use(express.static('public'))

		server.use(useragent.express())

		server.use((req,res,next) => {
			if(!isProd){
				console.log(`\n new request made:\n`,
				`host: ${req.hostname}\n`,
				`path: ${req.path}\n`,
				`method: ${req.method}\n`)
			}
			req.startTimeRequestLog = process.hrtime();
			server.locals.isMobile = (req.useragent.isMobile ? (req.useragent.isTablet ? false : true) : false);
			server.locals.isTablet = req.useragent.isTablet;

			/* Redirect if IP it's in blacklist
			if(BLACKLIST.indexOf(getClientIp(req)) !== -1){
				res.redirect(server.locals.web.url)
			} */
			
			next()
		})
		
		//Routes
		server.use('/', homeRouter)
		
		//Legal Texts
		server.get('/privacy-policy', async(req,res,next) => {
			res.render('privacy-policy', {title: `${req.webName} - Privacy Polciy`})
		})

		server.get('/terms-and-conditions', async(req,res,next) => {
			res.render('terms-and-conditions', {title: `${req.webName} - Terms And Conditions`})
		})
		
		//Api
		/*server.use(express.json())
		server.use('/api', api)*/

		//404 page
		server.get('*', (req, res) => {
			res.redirect('/')
			res.end()
		})

	}catch(err){
		console.log(err)
	}
}

startServer()

//helmet
/*server.use(helmet({
    contentSecurityPolicy: {
      directives: {
        ...helmet.contentSecurityPolicy.getDefaultDirectives(),
		"img-src": ["'self'", "https: data:"],
		"default-src": ["'self'", "https: data:"],
		"script-src": ["'self'", "https: data: 'unsafe-inline' 'unsafe-eval'"]
      },
    },
}))*/